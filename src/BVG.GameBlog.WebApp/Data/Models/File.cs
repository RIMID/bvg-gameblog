﻿using System.Collections.Generic;

using BVG.GameBlog.WebApp.Data.Interfaces;

namespace BVG.GameBlog.WebApp.Data.Models
{
    public class File : IModel<int>
    {
        public int Id { get; set; }

        public string Path { get; set; }
        public string Hash { get; set; }

        public ICollection<Post> PostPreviews { get; set; }
        public ICollection<User> AvatarUsers { get; set; }
    }
}
