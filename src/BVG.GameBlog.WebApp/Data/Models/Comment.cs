﻿using System;
using System.Collections.Generic;

using BVG.GameBlog.WebApp.Data.Interfaces;

namespace BVG.GameBlog.WebApp.Data.Models
{
    public class Comment : IModel<int>, ICreatedModel
    {
        public int Id { get; set; }

        public string Body { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }

        public int CreatedByUserId { get; set; }
        public User CreatedByUser { get; set; }
        public DateTime CreatedDate { get; set; }

        public ICollection<UserCommentLike> UserCommentLikes { get; set; }
    }
}
