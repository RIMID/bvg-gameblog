﻿using System.Collections.Generic;

using Microsoft.AspNetCore.Identity;

namespace BVG.GameBlog.WebApp.Data.Models
{
    public class User : IdentityUser
    {
        public int AvatarFileId { get; set; }
        public File AvatarFile { get; set; }

        public ICollection<Comment> CreatedComments { get; set; }
        public ICollection<Post> CreatedPosts { get; set; }

        public ICollection<UserPostLike> UserPostLikes { get; set; }
        public ICollection<UserCommentLike> UserCommentLikes { get; set; }
        public ICollection<UserPostView> UserPostViews { get; set; }
    }
}
