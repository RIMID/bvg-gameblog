﻿using System;
using System.Collections.Generic;

using BVG.GameBlog.WebApp.Data.Interfaces;

namespace BVG.GameBlog.WebApp.Data.Models
{
    public class Post : IModel<int>, ICreatedModel
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Body { get; set; }
        public bool CommentEnabled { get; set; }

        public int CreatedByUserId { get; set; }
        public User CreatedByUser { get; set; }
        public DateTime CreatedDate { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public int PreviewFileId { get; set; }
        public File PreviewFile { get; set; }

        public ICollection<UserPostLike> UserPostLikes { get; set; }
        public ICollection<UserPostView> UserPostViews { get; set; }
        public ICollection<PostTag> PostTags { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
