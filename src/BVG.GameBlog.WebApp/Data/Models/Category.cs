﻿using BVG.GameBlog.WebApp.Data.Interfaces;

using System.Collections.Generic;

namespace BVG.GameBlog.WebApp.Data.Models
{
    public class Category : IModel<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<Post> Posts { get; set; }
    }
}
