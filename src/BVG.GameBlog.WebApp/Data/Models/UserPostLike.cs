﻿namespace BVG.GameBlog.WebApp.Data.Models
{
    public class UserPostLike
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public int PostId { get; set; }
        public Post Post { get; set; }

        public bool IsLiked { get; set; }
    }
}
