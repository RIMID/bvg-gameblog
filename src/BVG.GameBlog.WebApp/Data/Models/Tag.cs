﻿using System.Collections.Generic;

using BVG.GameBlog.WebApp.Data.Interfaces;

namespace BVG.GameBlog.WebApp.Data.Models
{
    public class Tag : IModel<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<PostTag> PostTags { get; set; }
    }
}
