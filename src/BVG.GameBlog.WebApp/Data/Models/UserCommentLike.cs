﻿namespace BVG.GameBlog.WebApp.Data.Models
{
    public class UserCommentLike
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public int CommentId { get; set; }
        public Comment Comment { get; set; }

        public bool IsLiked { get; set; }
    }
}
