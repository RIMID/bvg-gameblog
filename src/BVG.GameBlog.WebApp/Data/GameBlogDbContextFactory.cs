﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

using BVG.GameBlog.WebApp.Data.Contexts;

namespace BVG.GameBlog.WebApp.Data
{
    public class GameBlogDbContextFactory : IDesignTimeDbContextFactory<GameBlogDbContext>
    {
        public GameBlogDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration.GetConnectionString("GameBlog");

            var builder = new DbContextOptionsBuilder<GameBlogDbContext>().UseSqlServer(connectionString);

            return new GameBlogDbContext(builder.Options);
        }
    }
}
