﻿using BVG.GameBlog.WebApp.Data.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BVG.GameBlog.WebApp.Data.Contexts
{
    public partial class GameBlogDbContext
    {
        public DbSet<PostTag> PostTags { get; set; }

        private void ConfigurePostTags(EntityTypeBuilder<PostTag> builder)
        {
            builder.HasKey(p => new { p.PostId, p.TagId });

            builder.HasOne(p => p.Post)
                .WithMany(p => p.PostTags)
                .HasForeignKey(p => p.PostId);

            builder.HasOne(p => p.Tag)
                .WithMany(p => p.PostTags)
                .HasForeignKey(p => p.TagId);
        }
    }
}
