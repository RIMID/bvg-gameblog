﻿using BVG.GameBlog.WebApp.Data.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BVG.GameBlog.WebApp.Data.Contexts
{
    public partial class GameBlogDbContext
    {
        public DbSet<File> Files { get; set; }

        private void ConfigureFiles(EntityTypeBuilder<File> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Path)
                .HasMaxLength(200)
                .IsRequired();

            builder.Property(p => p.Hash)
                .HasMaxLength(200)
                .IsRequired();
        }
    }
}
