﻿using BVG.GameBlog.WebApp.Data.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BVG.GameBlog.WebApp.Data.Contexts
{
    public partial class GameBlogDbContext
    {
        public DbSet<UserPostLike> UserPostLikes { get; set; }

        private void ConfigureUserPostLikes(EntityTypeBuilder<UserPostLike> builder)
        {
            builder.HasKey(p => new { p.UserId, p.PostId });

            builder.HasOne(p => p.Post)
                .WithMany(p => p.UserPostLikes)
                .HasForeignKey(p => p.PostId);

            builder.HasOne(p => p.User)
                .WithMany(p => p.UserPostLikes)
                .HasForeignKey(p => p.UserId);
        }
    }
}
