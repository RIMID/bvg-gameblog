﻿using BVG.GameBlog.WebApp.Data.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BVG.GameBlog.WebApp.Data.Contexts
{
    public partial class GameBlogDbContext
    {
        public DbSet<Post> Posts { get; set; }

        private void ConfigurePosts(EntityTypeBuilder<Post> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Title)
                .HasMaxLength(500)
                .IsRequired();

            builder.Property(p => p.Body)
                .IsRequired();

            builder.HasOne(p => p.CreatedByUser)
                .WithMany(p => p.CreatedPosts)
                .HasForeignKey(p => p.CreatedByUserId);

            builder.HasOne(p => p.PreviewFile)
                .WithMany(p => p.PostPreviews)
                .HasForeignKey(p => p.PreviewFileId);

            builder.HasOne(p => p.Category)
                .WithMany(p => p.Posts)
                .HasForeignKey(p => p.CategoryId);
        }
    }
}
