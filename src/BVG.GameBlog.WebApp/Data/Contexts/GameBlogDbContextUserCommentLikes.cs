﻿using BVG.GameBlog.WebApp.Data.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BVG.GameBlog.WebApp.Data.Contexts
{
    public partial class GameBlogDbContext
    {
        public DbSet<UserCommentLike> UserCommentLikes { get; set; }

        private void ConfigureUserCommentLikes(EntityTypeBuilder<UserCommentLike> builder)
        {
            builder.HasKey(p => new { p.CommentId, p.UserId });

            builder.HasOne(p => p.User)
                .WithMany(p => p.UserCommentLikes)
                .HasForeignKey(p => p.UserId);

            builder.HasOne(p => p.Comment)
                .WithMany(p => p.UserCommentLikes)
                .HasForeignKey(p => p.CommentId);
        }
    }
}
