﻿using BVG.GameBlog.WebApp.Data.Models;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BVG.GameBlog.WebApp.Data.Contexts
{
    public partial class GameBlogDbContext : IdentityDbContext<User, Role, string>
    {
        public GameBlogDbContext(DbContextOptions<GameBlogDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>(ConfigureUsers);
            builder.Entity<Post>(ConfigurePosts);
            builder.Entity<PostTag>(ConfigurePostTags);
            builder.Entity<Tag>(ConfigureTags);
            builder.Entity<Category>(ConfigureCategories);
            builder.Entity<Comment>(ConfigureComments);
            builder.Entity<File>(ConfigureFiles);
            builder.Entity<UserCommentLike>(ConfigureUserCommentLikes);
            builder.Entity<UserPostLike>(ConfigureUserPostLikes);
            builder.Entity<UserPostView>(ConfigureUserPostViews);
        }

        private void ConfigureUsers(EntityTypeBuilder<User> builder)
        {
            builder.HasOne(p => p.AvatarFile)
                .WithMany(p => p.AvatarUsers)
                .HasForeignKey(p => p.AvatarFileId);
        }
    }
}
