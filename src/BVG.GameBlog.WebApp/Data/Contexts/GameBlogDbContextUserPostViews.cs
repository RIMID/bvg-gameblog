﻿using BVG.GameBlog.WebApp.Data.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BVG.GameBlog.WebApp.Data.Contexts
{
    public partial class GameBlogDbContext
    {
        public DbSet<UserPostView> UserPostViews { get; set; }

        private void ConfigureUserPostViews(EntityTypeBuilder<UserPostView> builder)
        {
            builder.HasKey(p => new { p.PostId, p.UserId });

            builder.HasOne(p => p.Post)
                .WithMany(p => p.UserPostViews)
                .HasForeignKey(p => p.PostId);

            builder.HasOne(p => p.User)
                .WithMany(p => p.UserPostViews)
                .HasForeignKey(p => p.UserId);
        }
    }
}
