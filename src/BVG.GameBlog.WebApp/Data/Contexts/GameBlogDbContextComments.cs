﻿using BVG.GameBlog.WebApp.Data.Models;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BVG.GameBlog.WebApp.Data.Contexts
{
    public partial class GameBlogDbContext
    {
        public DbSet<Comment> Comments { get; set; }

        private void ConfigureComments(EntityTypeBuilder<Comment> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Body)
                .HasMaxLength(2000);

            builder.HasOne(p => p.Post)
                .WithMany(p => p.Comments)
                .HasForeignKey(p => p.PostId);

            builder.HasOne(p => p.CreatedByUser)
                .WithMany(p => p.CreatedComments)
                .HasForeignKey(p => p.CreatedByUserId);
        }
    }
}
