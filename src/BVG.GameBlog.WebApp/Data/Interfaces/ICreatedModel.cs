﻿using BVG.GameBlog.WebApp.Data.Models;
using System;

namespace BVG.GameBlog.WebApp.Data.Interfaces
{
    internal interface ICreatedModel
    {
        int CreatedByUserId { get; set; }
        User CreatedByUser { get; set; }

        DateTime CreatedDate { get; set; }
    }
}
