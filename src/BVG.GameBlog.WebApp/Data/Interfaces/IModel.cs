﻿namespace BVG.GameBlog.WebApp.Data.Interfaces
{
    internal interface IModel<TKey>
    {
        TKey Id { get; set; }
    }
}
